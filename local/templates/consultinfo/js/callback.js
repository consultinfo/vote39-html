

$(document).ready(function(){

    /*** [0] ***/

    mm_navigation();

    /*** [0] ***/

    var select = $('.mm_select select');
    if (select.size())
    {
        select.styler();
    }

    var mm_banner = $('#mm_banner'),
        timeout = mm_banner.data('timeout'),
        autoplay = 7000;

    if ( mm_banner.size() ){

        if (timeout != "") {
            autoplay = timeout;
        }

        mm_banner.owlCarousel({
            items: 1,
            autoplay: true,
            autoplayTimeout: autoplay,
            loop: true,
            nav: false,
            dots: false,
            mouseDrag: false,
            responsiveClass: true
        });
    }

    var stats = $('.mm_stats_list'),
        stats_res  = 991,
        stats_nav  = true,
        stats_dot  = false;
    mm_enable_carousel(stats,stats_res,stats_nav,stats_dot);

    var expert = $('#mm_expert_list'),
        expert_res  = 481,
        expert_nav  = true,
        expert_dot  = false;
    mm_enable_carousel(expert,expert_res,expert_nav,expert_dot);

    /*** [0] ***/

    mm_customScroll();

    /*** [0] ***/

    var wysiwyg = $('#mm_wysiwyg__preview, #mm_wysiwyg__full');

    if ( wysiwyg.size() ) 
    {
        wysiwyg.trumbowyg({
            lang: 'ru',
            svgPath: '/local/templates/consultinfo/images/trumbowyg/icons.svg'
        });
    }

    /*** [0] ***/

    if($('#birthdate').size())
    {
        var calendar = $('#birthdate')
                .datepicker({dateFormat: 'dd.mm.yyyy', changeMonth: true, changeYear: true})
                .data('datepicker');

        $('#show_date').on('click', function(){
            calendar.show();
        });
    }

    /*** [0] ***/
    var upSess = function(){
        $.ajax({
            type: "GET",
            url: "/api/1.0/sess",
            dataType: "json",
            data: {URL: window.location.toString()},
            success: function(ret){
                if(ret.error){
                    console.error(ret);
                }else{
                    //console.log(ret);
                }
            }
        });
    }

    setInterval(upSess, 60*1000 * 5);
    upSess();


    /*** [0] ***/
    /** pagen */
    var moreLink = $(".mm_section .mm_more_link");
    var nextButton = $(".mm_default_button", moreLink);

    if(nextButton.length > 0)
    {
        pp = $(".mm_more_link .pagination_wrp a[data-ajax-id]");
        if(pp.length > 0)
        {
            nextButton.show();
        }
        else if(pp.parents(".mm_more_pager_title").length==0)
        {
            //nextButton.hide();
        }
    }

    $(".mm_section").on('click', '.mm_more_link .mm_default_button', function(ev){
        var pp = $(".mm_section .mm_more_link .pagination_wrp a[data-ajax-id]");
        if(pp.length > 0)
        {
            var a = $(pp[0]);
            var ajaxId = a.data("ajax-id");

            moreLink.addClass("loading");
            var voteFilter = $("#vote_filter");
            if(voteFilter.length > 0)
            {
                var voteFilterFields = $(":checkbox, :radio", voteFilter);

                var req = {};
                voteFilterFields.each(function(){
                    var key = $(this).data("name");
                    var id = $(this).data("value");
                    if($(this).is(":checked")){
                        hasData = true;
                        if(typeof(req[key]) == "undefined"){
                            req[key] = [];
                        }
                        req[key].push(id);
                    }
                });

                $.ajax({
                    type: "POST",
                    url: a.attr("href")+"&bxajaxid="+ajaxId,
                    data: req,
                    dataType: "html",
                    success: function(data){
                        $("#comp_"+ajaxId+" .mm_list").append(data);
                        a.data("ajax-id",null).removeAttr("data-ajax-id");
                        pp = $(".mm_more_link .pagination_wrp a[data-ajax-id]");
                        if(pp.length > 0){
                            nextButton.show();
                        }else{
                            nextButton.hide();
                        }
                        moreLink.removeClass("loading");
                    }
                });

            }
            else
            {
                $.ajax({
                    type: "GET",
                    url: a.attr("href")+"&bxajaxid="+ajaxId,
                    dataType: "html",
                    success: function(data){
                        $("#comp_"+ajaxId+" .mm_list" ).append(data);
                        a.data("ajax-id",null).removeAttr("data-ajax-id");
                        pp = $(".mm_more_link .pagination_wrp a[data-ajax-id]");
                        if(pp.length > 0){
                            nextButton.show();
                        }else{
                            nextButton.hide();
                        }
                        moreLink.removeClass("loading");
                    }
                });
            }
        }
    });
    

    /*** [0] ***/
    /** vote list filter */
    var voteFilter = $("#vote_filter");
    if(voteFilter.length > 0)
    {
        var voteFilterFields = $(":checkbox, :radio", voteFilter);
        var voteFilterAppliedValue = $("#vote_filter_applied_value");

        var voteFilterFieldsChange = function()
        {
            var req = {}, hasData = false, hasDataShow = false;
            voteFilterFields.each(function(){
                var key = $(this).data("name");
                var id = $(this).data("value");
                if($(this).is(":checked")){
                    hasData = true;
                    if(typeof(req[key]) == "undefined"){
                        req[key] = [];
                    }
                    req[key].push(id);

                    if(key!="status" && $("#"+key+"-"+id, voteFilterAppliedValue).length == 0)
                    {
                        var name = $(this).next(".mm_checkbox__value").text().trim();
                        voteFilterAppliedValue.append(tpl('applied_filter',{key:key, id:id, name:name}));
                    }
                }else{
                    if(key!="status" && $("#"+key+"-"+id, voteFilterAppliedValue).length > 0)
                    {
                        $("#"+key+"-"+id, voteFilterAppliedValue).remove();
                    }
                }
            });
            if($(".mm_applied_filter_element", voteFilterAppliedValue).length > 0){
                voteFilterAppliedValue.parent().show();
            }else{
                voteFilterAppliedValue.parent().hide();
            }


            //if(hasData)
            {
                var ajaxId = voteFilter.data("ajax-id");
                var listURL = voteFilter.data("list-url");
                listURL = listURL || "/votes/";
                
                $("#comp_"+ajaxId+" .mm_list").addClass("loading");

                $.ajax({
                    type: "POST",
                    url: listURL+"?bxajaxid="+ajaxId,
                    data: req,
                    dataType: "html",
                    success: function(data){
                        $("#comp_"+ajaxId+" .mm_list").html(data);
                        $("#comp_"+ajaxId+" .mm_list").removeClass("loading");

                        var pager = $("#comp_"+ajaxId+" .mm_list #pager");

                        if(pager.length > 0)
                        {
                            var more_link = $("#comp_"+ajaxId+" .mm_more_link .pagination_wrp");
                            more_link.empty();

                            pager.detach().appendTo(more_link);

                            pp = $(".mm_more_link .pagination_wrp a[data-ajax-id]");
                            if(pp.length > 0){
                                nextButton.show();
                            }else{
                                nextButton.hide();
                            }
                        }
                    }
                });
            }
        };

        voteFilterFields.change(voteFilterFieldsChange);
        voteFilterAppliedValue.on('click', '.mm_applied_filter_element__remove', function(){
            var key = $(this).data("key");
            var id = $(this).data("id");
            $("#"+key+"-"+id, voteFilter).attr("checked", false);
            $("#"+key+"-"+id, voteFilterAppliedValue).remove();
            voteFilterFieldsChange();
        });
    }

    /*** [0] ***/
    /** vote list filter */
    $(".mm_vote_choice").on('change', '.mm_vote_option__input, .mm_choice_control__input', function(ev){
        $('#make_vote').attr('disabled', false);
    });
    $(".mm_vote_choice").on('keydown', '.mm_choice_control__text, .mm_choice_control__textarea', function(ev){
        $('#make_vote').attr('disabled', false);
    });
    

    /** vote form */

    var voteForm = $('form.vote-form');
    if (voteForm.size()) 
    {
        voteForm.unbind('submit').bind('submit', vote_form_submit);
    }

    /*** [0] ***/
    $('input[data-type=phone]').mask('7(999) 999-99-99');
    $('input[data-type=date]').mask('99.99.9999');


    /*** [0] ***/
    $('.mm_expert_grade__element').click(function(ev){
        ev.stopPropagation();
        ev.preventDefault();
        descs.hide();
        var desc = $(this).find(".mm_expert_grade__element_desc");
        if(desc.length > 0){
            desc.css('visibility','visible').show();
        }
    });
    var descs = $('.mm_expert_grade__element .mm_expert_grade__element_desc');
    if(descs.length > 0) {
        $(document).click(function(){
            descs.hide();
        });
        descs.css({'visibility':'visible', 'display':'none'});
    }


    $('body').on( 'click', '.captcha_reload', function(e){
        e.preventDefault();
        var row = $(this).parents('.captcha-form-item');
        $.ajax({
            url: '/api/1.0/captcha'
        }).done(function(text){
            row.find('input[name=captcha_sid]').val(text.code);
            row.find('img').attr('src', '/bitrix/tools/captcha.php?captcha_sid=' + text.code);
            row.find('input[name=captcha_word]').val('').removeClass('error');
            //row.find('.captcha_input').removeClass('error').find('.error').remove();
        });
    });


	/*** [yellow alert] ***/
	var yellowAlert = $(".urgent_messages_wrp");
	if(yellowAlert.length > 0) {
		var hideAlert = getCookie('hideAlert');
		if (!!hideAlert && hideAlert == 'Y') {
			yellowAlert.hide();
		}
		else {
			yellowAlert.show();
		}

		$(".mm_modal__close a", yellowAlert).click(function(){
			yellowAlert.hide();
		    setCookie('hideAlert','Y',{path:'/'});
		});
	}

});

$(document).scroll(function(){

}); //d.scroll

$(window).on('resize', function(){

    var stats = $('.mm_stats_list'),
        stats_res  = 991,
        stats_nav  = true,
        stats_dot  = false;
    mm_enable_carousel(stats,stats_res,stats_nav, stats_dot);

    var expert = $('#mm_expert_list'),
        expert_res  = 481,
        expert_nav  = true,
        expert_dot  = false;
    mm_enable_carousel(expert,expert_res,expert_nav,expert_dot);


}); //w.resize

//function navigation
function mm_navigation() 
{
    var website = $('body'),
        openLink = $('#navigation_open_link'),
        closeLink = $('#navigation_close_link, .mm_nav_overlay');

    openLink.on('click', function(){
        if (!website.hasClass('_nav_is_open')) {
            website.addClass('_nav_is_open');
            return(false);
        } else {
            website.removeClass('_nav_is_open');
            return(false);
        }
    });

    closeLink.on('click', function(){
        if (website.hasClass('_nav_is_open')) {
            website.removeClass('_nav_is_open');
            return(false);
        }
    });
}

//function dropdown
function mm_dropdown(el) 
{
    var link = $(el),
        dropdown = link.parents('.mm_dropdown'),
        close = dropdown.find('.mm_dropdown_window__close a'),
        overlay = dropdown.find('.mm_dropdown__overlay'),
        win = dropdown.find('.mm_dropdown_window'),
        noscroll = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
        body = $('body');

    var stopEv = function(e) {e.preventDefault()};

    //document.addEventListener('touchstart', stopEv, false);
    document.addEventListener('touchmove', stopEv, false);

    dropdown.stop().toggleClass('_active');
    if(noscroll) body.addClass('_noscroll');

    var H = body.height(),
        h = win.height(),
        t = (link.offset().top + link.height()) + 30,
        s = $(window).scrollTop(),
        d = (H + s) - (t + h);

    if(d < 0) {
        win.css({ "top": Math.round(d)+"px"});
    }
    else if(t < s) {
        win.css({ "top": Math.round(s - t)+"px"});
    }

    var closeHandler = function(e){
        e.stopPropagation();
        e.preventDefault();
        dropdown.removeClass('_active');
        body.removeClass('_noscroll');
        win.css({"top": 0});
        document.removeEventListener('touchmove', stopEv, false);
    };

    close.on('click touch tap', closeHandler);
    overlay.on('click touch tap', closeHandler);
}

//Enable owl.carousel
function mm_enable_carousel(id,resolution,nav,dots) 
{
    var screen_width = $(window).width();

    if ( screen_width <= resolution ) {

        if ( id.size() ) {
            id.addClass('owl-carousel');
            id.owlCarousel({
                items: 1,
                loop: false,
                nav: nav,
                navText: ['', ''],
                dots: dots,
                responsiveClass: true,
                autoHeight:true
            });
        }

    }
    else if (id.hasClass('owl-carousel')) {
        id.trigger('destroy.owl.carousel');
        id.removeClass('owl-carousel owl-loaded owl-drag');
        id.find('.item').appendTo(id);
        id.find('.owl-stage-outer').remove();
        id.find('.owl-nav').remove();
        id.find('.owl-dots').remove();
    }
}

//custom scroll
/*
function mm_customScroll(  popup ) {
    var scrollbar = $('._scrollbar'),
        opt = {
            skin: "default-skin",
            hScroll: false,
            updateOnWindowResize: true
        };

    if (window.innerWidth <= amberPopup.options.widthBreak) {
        popup.style.height = window.innerHeight + "px";
        scrollbar.css("height", "100%");
        opt.setHeight = window.innerHeight;
    }

    if ( scrollbar.size() ) {
        scrollbar.customScrollbar(opt);
    }
}*/
function mm_customScroll() 
{
    var scrollbar = $('._scrollbar');
    if ( scrollbar.size() ) {
        scrollbar.customScrollbar({
            skin: "default-skin",
            hScroll: false,
            updateOnWindowResize: true
        });
    }
}

//show more text
function mm_showMore(el) 
{
    var expand    =  el.data('expand'),
        collapse  =  el.data('collapse');
        div = el.data('div');

    el.toggleClass('_expanded');
    if ( el.hasClass('_expanded') && $(div).css('display') === 'none' ) {
        $(div).slideDown(350);
        el.html(expand);
    }
    else {
        $(div).slideUp(450);
        el.html(collapse);
        el.removeClass('_expanded');
    }
}



/*
    Function: tpl
    Функция-шаблонизатор с с динамической подгрузкой шаблонов и кешированием.

    Шаблоны определяются тегом «scripts», при этом, имя шаблона 
    должно заканчиваться на «-tpl», при вызове «-tpl» указывать 
    не надо:
    
    <script type="text/html" id="preview-tpl"></script>

    tpl('preview', { a: 'test1', 'b': 'test2'});
    
    Parameters:
        name - имя шаблона.
        data - данные, которые будут использованы для компиляции.

    Returns:
        Возвращает скомпилированную строку из шаблона и исходных данных.
*/
(function() {
    var cache = {};
    this.tpl = function (name, data) {
        if ( !cache[name] ) {
            var template = document.getElementById(name+'-tpl');
            if ( template ) {
                cache[name] = template.innerHTML;
            }
        }
        if ( cache[name] ) {
            return cache[name].replace(/\{([\w\.]*)\}/g, function (str, key) {
              var keys = key.split("."), value = data[keys.shift()];
              $.each(keys, function () { value = value[this]; });
              return (value === null || value === undefined) ? "" : value;
            });
        } else {
            console.error('Template ['+name+'] not found!');
            return(false);
        }
    }
})();


// vote_form_submit
vote_form_submit = function (ev)
{
    if(typeof(ev)!="undefined") ev.preventDefault();
    if(typeof(ev)!="undefined") ev.stopPropagation();

    $(this).addClass("loading");
    //$(this).find("[type='submit']").attr("disabled",true).attr("value","Отправляю...");
    $(this).find("button#make_vote").attr("disabled",true).html("Отправляю...");

    $(this).ajaxSubmit({
        url: '/api/1.0/vote',
        dataType: 'json',
        method: 'POST',
        success: $.proxy(function(ret){

            $(this).removeClass("loading");
            //$(this).find("[type='submit']").attr("disabled",false).attr("value","Отправить");
            $(this).find("button#make_vote").attr("disabled",true).html("Принято");

            if(typeof(ret) != "object")
            {
                console.error(ret);
                //alert_error(ret);
            }
            else if(ret.error)
            {
                //console.error(ret.error);
                if(ret.error.message)
                {
                    //amberPopup('#vote_error', {widthBreak: 0, onOpen: function(){ $("#vote_error .mm_vote_reiceved_message").html(ret.error.message);} });
                    alert_error(ret.error.message);
                }
                else
                {
                    //amberPopup('#vote_error', {widthBreak: 0, onOpen: function(){ $("#vote_error .mm_vote_reiceved_message").html(ret.error);} });
                    alert_error(ret.error);
                }
            }
            else
            {
                //console.log(ret);
                //amberPopup('#vote_received', {widthBreak: 0});
                alert_success("Спасибо<br>за ваш голос","Спасибо!");

                $(this).find("input").attr("disabled",true);

                if(!!ret.vote_result)
                {
                    $("#voting-form-result")
						.empty()
						.removeClass("mm_vote_choice")
                    	.html(ret.vote_result);
                }
            }
        },this)
    });
};


alert_error = function (message,title)
{
	message = typeof message !== 'undefined' ? message : "";
	title = typeof title !== 'undefined' ? title : "Внимание!";

	if($("#alert-error").size()==0)
	{
		$("#alert-error").remove();
		$(".mm_website_wrapper").append( tpl('alert-error', {"message": message, "title":title}) );
	}

	amberPopup('#alert-error', {
		widthBreak: 0,
		onOpen: function(){
			$("#alert-success").attr("title", title);
			$("#alert-error .mm_vote_reiceved_message").html(message);
		}
	});
};

alert_success = function (message,title)
{
	message = typeof message !== 'undefined' ? message : "";
	title = typeof title !== 'undefined' ? title : "Успешно!";

	if($("#alert-success").size()==0)
	{
		$("#alert-success").remove();
		$(".mm_website_wrapper").append( tpl('alert-success', {"message": message, "title":title}) );
	}

	amberPopup('#alert-success', {
		widthBreak: 0,
		onOpen: function(){
			$("#alert-success").attr("title", title);
			$("#alert-success .mm_vote_reiceved_message").html(message);
		}
	});
};


getCookie = function (name)
{
	var matches = document.cookie.match(new RegExp(
		"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	));
	return matches ? decodeURIComponent(matches[1]) : undefined;
};
setCookie = function (name, value, options)
{
	options = options || {};

	var expires = options.expires;

	if (typeof expires == "number" && expires) {
		var d = new Date();
		d.setTime(d.getTime() + expires * 1000);
		expires = options.expires = d;
	}
	if (expires && expires.toUTCString) {
		options.expires = expires.toUTCString();
	}

	value = encodeURIComponent(value);

	var updatedCookie = name + "=" + value;

	for (var propName in options) {
		updatedCookie += "; " + propName;
		var propValue = options[propName];
		if (propValue !== true) {
			updatedCookie += "=" + propValue;
		}
	}

	document.cookie = updatedCookie;
};

