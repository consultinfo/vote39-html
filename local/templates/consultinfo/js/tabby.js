/* =============================================================

    Tabby v2.0
    Simple, mobile-first toggle tabs by Chris Ferdinandi
    http://gomakethings.com

    Free to use under the MIT License.
    http://gomakethings.com/mit/

 * ============================================================= */

$(document).ready(function(){


    $(document).on('click', 'a.mm_default_tabs__link, a.mm__tabs__link', function(e){
        e.preventDefault(); // Prevent default link behavior.
        var tabID = $(this).attr('data-target'); // Pull the data-target value as the tabID.
        if(tabID){
            var parentTabConteiner = $(this).parents(".mm_default_tabs");
            if(parentTabConteiner.size() > 0)
            {
                var tab = $("a.mm_default_tabs__link[data-target='"+tabID+"']",parentTabConteiner);
                /*$(this)*/ tab.addClass('_active').parent().addClass('_active'); // Add the ._active class to the link and it's parent li (if one exists).
                /*$(this)*/ tab.siblings().removeClass('_active'); // Remove the ._active class from sibling tab navigation elements.
                /*$(this)*/ tab.parent('li').siblings().removeClass('_active').children().removeClass('_active'); // Remove the ._active class from sibling li elements and their links.
                $(tabID, parentTabConteiner).addClass('_active'); // Add the ._active class to the div with the tab content.
                $(tabID, parentTabConteiner).siblings().removeClass('_active'); // Remove the ._active class from other tab content divs.
            }
        }
    });

});





/* =============================================================

    Progressively Enhanced JS v1.0
    Adds .js class to <body> for progressive enhancement.

    Script by Chris Ferdinandi.
    http://gomakethings.com

    Free to use under the MIT License.
    http://gomakethings.com/mit/

 * ============================================================= */
