/***
    The MIT License (MIT)
    http://dbogdanoff.ru/popup/
    Copyright (c) 2017 - Denis Bogdanov
    Kaliningrad, Russia
    version: 0.9
 ***/
;(function (window) {
    /**
     * Функция "контроллер", определяет тип переданного окна и запускает показ
     * Выбрасывает исключение в случае неизвестного типа для popupObject
     *
     * @param popupObject
     * @param params
     */
    var amberPopup = function ( popupObject, params ) {
        try {
            params = typeof params == "object" ? params : {};
            options = Object.assign({}, defaultOptions, params);
            amberPopup.options = options;

            if ( isElement( popupObject ) ) {
                popup = popupObject.cloneNode(true);
                showPopup();
            }
            else if ( typeof popupObject == "string" )
            {
                if ( popupObject.indexOf("/") >= 0 || options.ajax == true) {
                    options.ajax = true;
                    if (options.changeUri === true) {
                        options.history = true;
                        options.url = popupObject;
                    }
                    else {
                        options.url = window.location.href;
                    }

                    amberFetch( popupObject, params );
                }
                else {
                    popupObject = document.querySelector( popupObject );
                    if ( isElement(popupObject) ) {
                        popup = popupObject.cloneNode(true);
                        showPopup();
                    }
                    else {
                        throw new Error("Element Not Found");
                    }
                }
            }
            else {
                throw new Error("The first argument is not valid");
            }
        }
        catch(e) {
            console.log(e);
        }
    };

    /**
     * Установка глобальных опций плагиа
     * @param params
     */
    amberPopup.init = function ( params ) {
        defaultOptions = Object.assign({}, defaultOptions, params);
    };

    /**
     * Закрытие окна
     */
    amberPopup.close = function () {
        if (block == true) return false;
        if (options.history === true) {
            window.history.back();
        }
        else {
            closeReal();
        }
    };

    /**
     * Функция запускает показ оверлея и вставки контейнера
     */
    function showPopup() {
        showOverlay();
        addContainer();
    }

    /**
     * Добавляет оверлей в body, применяет стили с options
     * @returns {boolean}
     */
    function showOverlay() {
        overlay = document.getElementById("amberPopup");

        if (!isElement( overlay )) {
            overlay = document.createElement("div");
            overlay.id = "amberPopup";
            overlay.className = "amberpopup";

            overlay.style.width = "100%";
            overlay.style.height = "100%";
            overlay.style.top = "0";
            overlay.style.left = "0";
            overlay.style.position = "fixed";
            overlay.style.overflowY = "scroll";
            overlay.style.textAlign = "center";

            document.body.appendChild(overlay);
            overlay.addEventListener("click", closeHandler);
            document.addEventListener("keydown", escHandler);
            window.addEventListener("resize", setPosition);
        }
        else {
            overlay.style.display = "block";
        }

        overlay.style.zIndex = getZindex();
        overlay.style.display = "block";

        overlay.style.transition = "background 0s cubic-bezier(.05,.7,.4,.9)";
        overlay.style.background = "rgba(0,0,0,0)";

        setTimeout(function () {
            overlay.style.transitionDuration = options.duration;
            overlay.style.background = getBackground();
        }, 4);
    }

    /**
     * Добавляет контейнер с содержимым окна в оверлей, применяет стили с options
     * @returns {boolean}
     */
    function addContainer() {
        // скрыть все текущие контейнеры
        var curContainers = overlay.querySelectorAll(".amberpopup__container");
        curContainers.forEach(function (item) {
            item.style.display = "none";
        });

        if (popup.tagName == "IMG") {
            var newImg = new Image();

            newImg.onload = function() {
                options.originWidth = newImg.width;
                setPosition();
            }

            newImg.src = popup.getAttribute("src");
            options.width = "auto";
        }

        var div = document.createElement("div");

        div.style.background = options.bContainer;
        div.style.margin = "10px auto 20px";
        div.style.position = "relative";
        div.style.display = "inline-block";
        div.style.webkitBoxShadow = options.boxShadow;
        div.className = "amberpopup__container";
        div.appendChild(popup);

        // вставить контейнер в оверлей
        overlay.appendChild(div);
        toggleScroll(false);
        setPosition();

        div.style.transition = "opacity 0s cubic-bezier(.05,.7,.4,.9)";
        div.style.opacity = "0";

        setTimeout(function () {
            div.style.transitionDuration = options.duration;
            div.style.opacity = "1";
            setPosition();
        }, 10);

        // save popup
        if (options.skipCount !== true && options.history === true) {
            defaultOptions.count++;
            defaultOptions.current = defaultOptions.count;
            deck[ defaultOptions.count ] = popup;
            var save_opt = {};
            for(var i in options) {
                if (!isFunction(options[i]) && typeof options[i] != "object") {
                    save_opt[i] = options[i];
                }
            }
            window.history.pushState({page: defaultOptions.count, opt: save_opt}, "", options.url);
        }

        if (isFunction(options.onOpen)) {
            options.onOpen( popup );
        }
    }

    /**
     * Позиционирование контейнера
     */
    function setPosition() {
        var top,
            left = "inherit",
            width = options.width,
            height = "auto",
            marginTop = "10px",
            marginBottom = "20px",
            curContainers = overlay.querySelectorAll(".amberpopup__container");

        if (curContainers.length) {
            curContainers = curContainers[ curContainers.length-1 ];
            top = window.innerHeight/2-curContainers.offsetHeight/2;
            top = (top < 0 ? 10 : top) + "px";

            if (window.outerWidth <= options.widthBreak) {
                top = 0;
                left = 0;
                width = "100%";
                height  = "100%";
                marginTop = 0;
                marginBottom = 0;
            }
            else if (curContainers.width >= window.innerWidth) {
                width = "100%"
            }
            else if (window.innerWidth >= options.originWidth) {
                width = options.width;
            }

            curContainers.style.top = top;
            curContainers.style.left = left;
            curContainers.style.width = width;
            curContainers.style.height = height;
            curContainers.style.marginTop = marginTop;
            curContainers.style.marginBottom = marginBottom;

            overlay.style.lineHeight = popup.tagName == "IMG" ? "0" : "inherit";
            curContainers.style.lineHeight = popup.tagName == "IMG" ? "0" : "inherit";
            if (popup.tagName == "IMG") {
                popup.style.width = "100%";
            }
        }
    }

    /**
     * Скрывает скролл для body и делает padding, чтобы стр. не прыгала
     */
    function toggleScroll(scroll) {
        if (scroll === true) {
            document.body.style.overflowY = "scroll";
            document.body.style.padding = "initial";
        }
        else {
            document.body.style.overflowY = "hidden";
            document.body.style.padding = "0 17px 0 0";
        }
    }

    /**
     * Закрытие окна
     * @returns {boolean}
     */
    function closeReal() {
        if (block == true) return false;
        var curContainers = overlay.querySelectorAll(".amberpopup__container");
        if (curContainers.length > 1) {
            curContainers[ curContainers.length-1 ].remove();
            curContainers[ curContainers.length-2 ].style.display = "block";
            setPosition();

            if (isFunction(options.onBack)) {
                options.onBack();
            }
        }
        else if (curContainers.length) {
            curContainers[ curContainers.length-1 ].style.transitionDuration = ".2s";
            curContainers[ curContainers.length-1 ].style.opacity = "0";
            overlay.style.transitionDuration = ".13s";
            overlay.style.background = "rgba(0,0,0,0)";
            block = true;

            setTimeout(function () {
                curContainers[ curContainers.length-1 ].remove();
                overlay.style.display = "none";
                toggleScroll(true);
                block = false;

                if (isFunction(options.onCloseAll)) {
                    options.onCloseAll();
                }
            }, 200);
        }

        if (isFunction(options.onClose)) {
            options.onClose();
        }
    }

    /**
     * Событие клик на оверлей, для закрытия окна
     * @param e
     */
    function closeHandler( e ) {
        if (overlay.getAttribute("id") == e.target.getAttribute("id")) {
            amberPopup.close();
        }
    }

    /**
     * Событие Esc на оверлей, для закрытия окна
     * @param e
     */
    function escHandler( e ) {
        if (!overlay.querySelectorAll(".amberpopup__container").length)
            return false;

        e = e || window.event;
        var isEscape = false;
        if ("key" in e) {
            isEscape = (e.key == "Escape" || e.key == "Esc");
        } else {
            isEscape = (e.keyCode == 27);
        }
        if (isEscape) {
            amberPopup.close();
        }
    }

    /**
     * Выполняет запрос и получает html-окна
     *
     * @param url
     * @param params
     */
    function amberFetch( url, params ) {
        var args    = fetchData(url, params),
            promise = fetch(args.url, args.options);

        promise.then(function (response) {
            if (response.status >= 200 && response.status < 300) {
                return response.text();
            } else {
                return Promise.reject(new Error(response.statusText));
            }
        })
            .then(function (html) {
                var call = true;
                if (isFunction(options.onLoad)) {
                    call = options.onLoad( html );
                }

                var div = document.createElement("div");
                div.innerHTML = html;

                if (call !== false) {
                    popup = div;
                    showPopup();
                }
            });

        promise.then(null, console.log);
    }

    /**
     * Формирование объекта данных для fetch-запроса
     *
     * @param url
     * @param params
     * @returns {{url: *, options: {method: (string|*|string), credentials: string}}}
     */
    function fetchData( url, params ) {
        var data,
            result = {
                url: url,
                options: {
                    method:         options.method,
                    credentials:    "include"
                }
            };

        if (typeof params.data == "object" && options.method == "post") {
            data = new FormData();
            for(var i in params) {
                data.append(i, params[i]);
            }

            result.options.body = data;
        }
        else if (typeof params.data == "object") {
            data = Object.keys(params.data).map(function(key){
                return encodeURIComponent(key) + "=" + encodeURIComponent(params.data[key]);
            }).join("&");

            if (url.indexOf("?") >= 0) {
                result.url += "&" + data;
            }
            else {
                result.url += "?" + data;
            }
        }

        var myHeaders = new Headers();
        if (options.cache !== true) {
            myHeaders.append("pragma", "no-cache");
            myHeaders.append("cache-control", "no-cache");
            result.options.headers  = myHeaders;
            result.options.cache    = "no-cache";
        }

        return result;
    }

    /**
     * Getter background overlay
     * @returns {string}
     */
    function getBackground() {
        if (!/^#([A-Fa-f0-9]{3}){1,2}$/.test(options.background)) {
            options.background = "#000";
        }
        if (!isNumeric(options.opacity)) {
            options.opacity = "0.6";
        }

        var c = options.background.substring(1).split("");
        if (c.length == 3) {
            c = [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c = "0x"+c.join("");
        return "rgba("+[(c>>16)&255, (c>>8)&255, c&255].join(",")+","+options.opacity+")";
    }

    /**
     * Getter zIndex
     * @returns {string}
     */
    function getZindex() {
        if (!isNumeric(options.zIndex)) {
            options.zIndex = "1900";
        }

        return options.zIndex;
    }

    /**
     * Проверка элемента
     * @param obj
     */
    function isElement( obj ) {
        try {
            return obj instanceof HTMLElement;
        }
        catch(e) {
            return (typeof obj==="object") &&
                (obj.nodeType===1) && (typeof obj.style === "object") &&
                (typeof obj.ownerDocument ==="object");
        }
    }

    /**
     * Проверяет, что аргумент является функцие
     *
     * @param functionToCheck
     * @returns {boolean}
     */
    function isFunction( functionToCheck ) {
        var getType = {};
        return functionToCheck && getType.toString.call(functionToCheck) === "[object Function]";
    }

    /**
     * Check numeric
     *
     * @param n
     * @returns {boolean}
     */
    function isNumeric( n ) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    // Массив со всеми окнами
    var deck = [];

    // Variables
    var overlay, popup, block, options, defaultOptions = {
        opacity:        "0.7",  // прозрачность оверлея
        background:     "#000", // фон оверлея
        bContainer:     "transparent", // фон контейнера
        zIndex:         "1900", // z-index оверлея
        width:          "100%",// ширина контейнера
        duration:       ".2s",  // скорость анимации показа оверлея
        boxShadow:      "none", // тень контейнера
        widthBreak:     768,

        content:        false,  // image | iframe

        position:       false,  // [x, y]
        modalClose:     false,  // разрешать закрытие при клике на оверлей
        autoClose:      false,  // автоматическое закрытие окна. скорее всего понадобится только локальное применение
        saveScroll:     true,   // сохранять ли скролл окна

        history:        false,   // сохранять ли открытие окна в истории баузера
        changeUri:      false,  // подставлять ли путь ajax-запросов в адресную строку, при true опция history автоматичестки выставится в true
        cache:          false,   // сохранять ли кеш запроса
        method:         "get",  // метод запроса
        loader:         true,  // selector или node анимационного загрузчика, который будет показан до ожидания загрузка ajax-запроса

        count:          0,      // системый, не будет описан в документации
        current:        0,      // системый, не будет описан в документации
        skipCount:      false,  // системый, не будет описан в документации

        onOpen:         function () {}, // Вызывается после показа окна
        onClose:        function () {}, // Вызывается после закрытия любого окна
        onLoad:         function () {}, // Вызывается после ajax-запроса и до открытия окна, вернув false в коде обработчика можно прервать показ окна
        onBack:         function () {}, // Вызывается после перехода к предыдущему окну, но когда окно не последнее в очереди
        onCloseAll:     function () {}  // Вызывается после перехода к предыдущему окну, когда окно последнее в очереди
    };

    /**
     * Обработка "прогулки" по истории браузера
     */
    window.addEventListener("popstate", function( e ) {
        if (e.state == null || e.state.page < defaultOptions.current) {
            defaultOptions.current = e.state != null ? e.state.page : 0;
            closeReal();
        }
        else if (e.state.page > defaultOptions.current) {
            if (deck[ e.state.page ]) {
                defaultOptions.current = e.state.page;
                amberPopup( deck[ e.state.page ], Object.assign(e.state.opt, {skipCount:true}) );
            }
        }
    }, false);

    window.amberPopup = amberPopup;
}(window));